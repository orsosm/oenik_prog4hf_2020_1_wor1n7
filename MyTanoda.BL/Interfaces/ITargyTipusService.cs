﻿// <copyright file="ITargyTipusService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Interfaces
{
    using System.Collections.Generic;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Public interface for TargyTipus service.
    /// </summary>
    public interface ITargyTipusService : IRepository<TargyTipus>
    {
        List<TargyTipus> GetAll();

        TargyTipus GetOne(int id);

        void Create(TargyTipus tt);

        void Update(TargyTipus tt);

        void Delete(int ttID);
    }
}
