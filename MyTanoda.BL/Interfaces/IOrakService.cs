﻿// <copyright file="IOrakService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Interfaces
{
    using System.Collections.Generic;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Public interface for Orak Service.
    /// </summary>
    public interface IOrakService : IRepository<Orak>
    {
        List<Orak> GetAll();

        Orak GetOne(int id);

        void Create(Orak o);

        void Update(Orak o);

        void Delete(int orakID);
    }
}
