﻿// <copyright file="IExtraFoglalkozasokService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Interfaces
{
    using System.Collections.Generic;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Public interface for ExtraFoglalkozasok Service.
    /// </summary>
    public interface IExtraFoglalkozasokService : IRepository<ExtraFoglalkozasok>
    {
        /// <summary>
        /// Gets All ExtraFoglalkozasok record.
        /// </summary>
        /// <returns>List of ExtraFoglalkozasok.</returns>
        List<ExtraFoglalkozasok> GetAll();

        /// <summary>
        /// Returns one record of ExtraFoglalkozasok.
        /// </summary>
        /// <param name="id">ID of the reqested record.</param>
        /// <returns>ExtraFoglalkozasok object.</returns>
        ExtraFoglalkozasok GetOne(int id);

        /// <summary>
        /// Creates a new ExtraFoglalkozasok record.
        /// </summary>
        /// <param name="ef">ExtraFoglalkozasok object.</param>
        void Create(ExtraFoglalkozasok ef);

        /// <summary>
        /// UPdates ExtraFoglalkozasok record.
        /// </summary>
        /// <param name="ef">The new record.</param>
        void Update(ExtraFoglalkozasok ef);

        /// <summary>
        /// Deletes ExtraFoglalkozasok record.
        /// </summary>
        /// <param name="efID">ID of ExtraFoglalkozasok.</param>
        void Delete(int efID);
    }
}
