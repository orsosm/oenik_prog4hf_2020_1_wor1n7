﻿// <copyright file="ITargyakService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Interfaces
{
    using System.Collections.Generic;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Public Interface for Targyak service.
    /// </summary>
    public interface ITargyakService : IRepository<Targyak>
    {
        List<Targyak> GetAll();

        Targyak GetOne(int id);

        void Create(Targyak t);

        void Update(Targyak t);

        void Delete(int targyID);
    }
}
