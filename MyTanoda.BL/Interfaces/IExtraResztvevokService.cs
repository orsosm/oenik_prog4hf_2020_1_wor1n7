﻿// <copyright file="IExtraResztvevokService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Interfaces
{
    using System.Collections.Generic;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Public interface for ExtraResztvevok service.
    /// </summary>
    public interface IExtraResztvevokService : IRepository<ExtraResztvevok>
    {
        List<ExtraResztvevok> GetAll();

        ExtraResztvevok GetOne(int id);

        void Create(ExtraResztvevok e);

        void Update(ExtraResztvevok e);

        void Delete(int eID);
    }
}
