﻿// <copyright file="IDiakService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Interfaces
{
    using System.Collections.Generic;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Interface to DiakService so Business Logic layer can access Data Layer.
    /// </summary>
    public interface IDiakService : IRepository<Diakok>
    {
        /// <summary>
        /// Returns All Diakok set in a List.
        /// </summary>
        /// <returns>List of Diakok type.</returns>
        new List<Diakok> GetAll();

        /// <summary>
        /// Returns one entity of Diakok.
        /// </summary>
        /// <param name="id">The ID of Diakok.</param>
        /// <returns>One entity of Diakok.</returns>.
        new Diakok GetOne(int id);

        /// <summary>
        /// Creates one record of Diakok type.
        /// </summary>
        /// <param name="d">Diakok object to be added to the DataBAse.</param>
        new void Create(Diakok d);

        /// <summary>
        /// Updates a Diakok object.
        /// </summary>
        /// <param name="d">Diakok object.</param>
        new void Update(Diakok d);

        /// <summary>
        /// Deletes a record from Diakok table.
        /// </summary>
        /// <param name="diakID">ID of the record to be deleted.</param>
        new void Delete(int diakID);
    }
}
