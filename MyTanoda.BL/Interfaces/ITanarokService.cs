﻿// <copyright file="ITanarokService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Interfaces
{
    using System.Collections.Generic;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Public interface for Tanarok service.
    /// </summary>
    public interface ITanarokService : IRepository<Tanarok>
    {
        List<Tanarok> GetAll();

        Tanarok GetOne(int id);

        void Create(Tanarok t);

        void Update(Tanarok t);

        void Delete(int tanarID);
    }
}
