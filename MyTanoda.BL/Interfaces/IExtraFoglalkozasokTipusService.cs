﻿// <copyright file="IExtraFoglalkozasokTipusService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Interfaces
{
    using System.Collections.Generic;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Public interface for ExtraFoglalkozasokTipus service.
    /// </summary>
    public interface IExtraFoglalkozasokTipusService : IRepository<ExtraFoglalkozasokTipus>
    {
        List<ExtraFoglalkozasokTipus> GetAll();

        ExtraFoglalkozasokTipus GetOne(int id);

        void Create(ExtraFoglalkozasokTipus ef);

        void Update(ExtraFoglalkozasokTipus ef);

        void Delete(int efID);
    }
}
