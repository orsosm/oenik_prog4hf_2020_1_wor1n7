﻿// <copyright file="IOrakTipusService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Interfaces
{
    using System.Collections.Generic;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Public interface for OrakTipus service.
    /// </summary>
    public interface IOrakTipusService : IRepository<OrakTipus>
    {
        List<OrakTipus> GetAll();

        OrakTipus GetOne(int id);

        void Create(OrakTipus o);

        void Update(OrakTipus o);

        void Delete(int oraTipusID);
    }
}
