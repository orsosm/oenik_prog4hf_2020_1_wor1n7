﻿// <copyright file="TargyTipusService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Public class for TargyTipus service.
    /// </summary>
    public class TargyTipusService
    {
        /// <summary>
        /// Gets or sets TargyTipus repository.
        /// </summary>
        /// <value>TargyTipus repo.</value>
        public IRepository<TargyTipus> TargyTipusRepository { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TargyTipusService"/> class.
        /// </summary>
        public TargyTipusService() => TargyTipusRepository = new TargyTipusRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="TargyTipusService"/> class.
        /// </summary>
        /// <param name="targyTipusRepo">TargyTipis Repository.</param>
        public TargyTipusService(IRepository<TargyTipus> targyTipusRepo) => this.TargyTipusRepository = targyTipusRepo;

        /// <summary>
        /// Provides service for TargyTopus repository => create.
        /// </summary>
        /// <param name="tt">TargyTipus object.</param>
        public void Create(TargyTipus tt) => TargyTipusRepository.Create(tt);

        /// <summary>
        /// Provides service for TargyTopus repository => Delete.
        /// </summary>
        /// <param name="ttId">ID to be deleted.</param>
        public void Delete(int ttId) => TargyTipusRepository.Delete(ttId);

        /// <summary>
        /// Provides service for TargyTopus repository => GetAll.
        /// </summary>
        /// <returns>List of TargyTipus.</returns>
        public List<TargyTipus> GetAll() => TargyTipusRepository.GetAll().ToList();

        /// <summary>
        /// Provides service for TargyTopus repository => Update.
        /// </summary>
        /// <param name="tt">TargyTipus object.</param>
        public void Update(TargyTipus tt) => TargyTipusRepository.Update(tt);

        /// <summary>
        /// Provides service for TargyTopus repository => GetOne.
        /// </summary>
        /// <param name="id">ID to be queried.</param>
        /// <returns>One record.</returns>
        public TargyTipus GetOne(int id) => TargyTipusRepository.GetOne(id);
    }
}
