﻿// <copyright file="ExtraResztvevokService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Public class for ExtraResztvevok service.
    /// </summary>
    public class ExtraResztvevokService
    {
        /// <summary>
        /// Gets or sets ExtraResztvevok repository.
        /// </summary>
        /// <value>ExtraResztvevokRepository.</value>
        public IRepository<ExtraResztvevok> ExtraResztvevokRepository { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtraResztvevokService"/> class.
        /// </summary>
        public ExtraResztvevokService() => ExtraResztvevokRepository = new ExtraResztvevokRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtraResztvevokService"/> class.
        /// </summary>
        /// <param name="eResztvevokRepo">ExtraResztvevok repository.</param>
        public ExtraResztvevokService(IRepository<ExtraResztvevok> eResztvevokRepo) => this.ExtraResztvevokRepository = eResztvevokRepo;

        /// <summary>
        /// Provides service for ExtraResztvevok repository => create.
        /// </summary>
        /// <param name="e">ExtraResztvevok object.</param>
        public void Create(ExtraResztvevok e) => ExtraResztvevokRepository.Create(e);

        /// <summary>
        /// Provides service for ExtraResztvevok repository => Delete.
        /// </summary>
        /// <param name="eId">ID to be deleted.</param>
        public void Delete(int eId) => ExtraResztvevokRepository.Delete(eId);

        /// <summary>
        /// Provides service for ExtraResztvevok repository => GetAll.
        /// </summary>
        /// <returns>List of ExtraResztvevok.</returns>
        public List<ExtraResztvevok> GetAll() => ExtraResztvevokRepository.GetAll().ToList();

        /// <summary>
        /// Provides service for ExtraResztvevok repository => Update.
        /// </summary>
        /// <param name="e">Extraresztvevok object.</param>
        public void Update(ExtraResztvevok e) => ExtraResztvevokRepository.Update(e);

        /// <summary>
        /// Provides service for ExtraResztvevok repository => GetOne.
        /// </summary>
        /// <param name="id">ID to be queried.</param>
        /// <returns>One record.</returns>
        public ExtraResztvevok GetOne(int id) => ExtraResztvevokRepository.GetOne(id);
    }
}
