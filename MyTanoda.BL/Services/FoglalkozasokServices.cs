﻿// <copyright file="FoglalkozasokServices.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using MyTanoda.DA;
    using MyTanoda.DA.Repositories;
    using MyTanoda.RP;
    using Newtonsoft.Json;

    /// <summary>
    /// Provides services for Foglalkozasok.
    /// </summary>
    public class FoglalkozasokServices
    {
        private IRepository<Diakok> diakokRepository;
        private IRepository<Tanarok> tanarokRepository;
        private IRepository<ExtraFoglalkozasok> extraFoglalkozasokRepository;
        private IRepository<ExtraFoglalkozasokTipus> extraFoglalkozasokTipusRepository;
        private IRepository<ExtraResztvevok> extraResztvevokRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="FoglalkozasokServices"/> class.
        /// </summary>
        /// <param name="diakokRepository">Instance of DiakokRepository.</param>
        /// <param name="tanarokRepository">Instance of TanarokRepository.</param>
        /// <param name="extraFoglalkozasokRepository">Instance of ExtraFoglalkozasokRepository.</param>
        /// <param name="extraFoglalkozasokTipusRepository">Instance of ExtraFoglalkozasokTipusRepository.</param>
        /// <param name="extraResztvevokRepository">Instance of ExtraResztvevokRepository.</param>
        public FoglalkozasokServices(
             DiakRepository diakokRepository,
             TanarokRepository tanarokRepository,
             ExtraFoglalkozasokRepository extraFoglalkozasokRepository,
             ExtraFoglalkozasokTipusRepository extraFoglalkozasokTipusRepository,
             ExtraResztvevokRepository extraResztvevokRepository)
        {
            this.diakokRepository = diakokRepository;
            this.tanarokRepository = tanarokRepository;
            this.extraFoglalkozasokRepository = extraFoglalkozasokRepository;
            this.extraFoglalkozasokTipusRepository = extraFoglalkozasokTipusRepository;
            this.extraResztvevokRepository = extraResztvevokRepository;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FoglalkozasokServices"/> class.
        /// </summary>
        /// <param name="diakokRepository">Instance of Diakok IRepository.</param>
        /// <param name="tanarokRepository">Instance of Tanarok IRepository.</param>
        /// <param name="extraFoglalkozasokRepository">Instance of ExtraFoglalkozasok IRepository.</param>
        /// <param name="extraFoglalkozasokTipusRepository">Instance of ExtraFoglalkozasokTipus IRepository.</param>
        /// <param name="extraResztvevokRepository">Instance of ExtraResztvevok IRepository.</param>
        public FoglalkozasokServices(
             IRepository<Diakok> diakokRepository,
             IRepository<Tanarok> tanarokRepository,
             IRepository<ExtraFoglalkozasok> extraFoglalkozasokRepository,
             IRepository<ExtraFoglalkozasokTipus> extraFoglalkozasokTipusRepository,
             IRepository<ExtraResztvevok> extraResztvevokRepository)
        {
            this.diakokRepository = diakokRepository;
            this.tanarokRepository = tanarokRepository;
            this.extraFoglalkozasokRepository = extraFoglalkozasokRepository;
            this.extraFoglalkozasokTipusRepository = extraFoglalkozasokTipusRepository;
            this.extraResztvevokRepository = extraResztvevokRepository;
        }

        /// <summary>
        /// Gets the participants' list of an ExtraFoglalkozas.
        /// </summary>
        /// <param name="efId">ExtraFoglalkozas ID.</param>
        /// <returns>The list of participants.</returns>
        public List<string> ExtraFoglalkozasokResztvevoDiakok(int efId)
        {
            var resztvevok = this.extraResztvevokRepository.GetAll().Where(x => x.extra_id == efId).ToArray();
            var ef = this.extraFoglalkozasokRepository.GetAll().Where(x => x.id == efId).ToArray();
            var efTipus = this.extraFoglalkozasokTipusRepository.GetAll().ToArray();

            var result = (from resztv in resztvevok
                          join diak in diakokRepository.GetAll() on resztv.diak_id equals diak.id
                          join efogl in ef on resztv.extra_id equals efogl.id
                          join eft in efTipus on efogl.tipus equals eft.id
                          select $"{diak.nev} - {efogl.nev} - {eft.nev} - {efogl.datum.Value.ToShortDateString()}").ToList();

            return result;
        }

        /// <summary>
        /// Gets the number of ExtraFoglalkozasok DiakID participates.
        /// </summary>
        /// <param name="diakId">Particiopant's ID.</param>
        /// <returns>The number of ExtraFoglalkozasok.</returns>
        public int ExtraFoglalkozasokonResztVeszSzam(int diakId)
        {
            var ef = this.extraResztvevokRepository.GetAll().Where(x => x.diak_id == diakId).ToArray();
            var result = (from diak in ef
                          join diakok in diakokRepository.GetAll() on diak.diak_id equals diakok.id
                          select diak.diak_id).Count();

            return result;
        }

        /// <summary>
        /// Gets the list of Tanarok accompanying at ExtraFoglalkozasok.
        /// </summary>
        /// <returns>LIst of accomanying teachers.</returns>
        public List<string> ExtraFoglalkozasokKiseroTanarok()
        {
            var efResztv = this.extraResztvevokRepository.GetAll().ToArray();
            var ef = this.extraFoglalkozasokRepository.GetAll().ToArray();
            var efTipus = this.extraFoglalkozasokTipusRepository.GetAll().ToArray();
            var tanRepo = this.tanarokRepository.GetAll().ToArray();

            var result = (from tanar in tanRepo
                          join resztv in efResztv on tanar.id equals resztv.tanar_id
                          join fogl in ef on resztv.extra_id equals fogl.id
                          join tipus in efTipus on fogl.tipus equals tipus.id
                          select $"{fogl.nev} - {tanar.nev}").Distinct().ToList();

            return result;
        }

        /// <summary>
        /// Calls Java Endpoint.
        /// </summary>
        /// <param name="info">How many ExtraFoglalkozas to be asked.</param>
        /// <returns>List of ExtraFoglalkozas in JSON format.</returns>
        public List<JavaDto> GetExtraFoglalkozasokFromJavaEndpoint(string info)
        {
           // string info = "10";
            HttpClient client = new HttpClient();
            HttpResponseMessage responseMessage = client.PostAsync("http://localhost:8080/extrafoglalkozasok/EFGenerator", new StringContent(info, Encoding.UTF8, "application/json")).Result;

            var result = responseMessage.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<List<JavaDto>>(result);
        }
    }
}