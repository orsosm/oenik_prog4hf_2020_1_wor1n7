﻿// <copyright file="OrakService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Public class for Orak service.
    /// </summary>
    public class OrakService
    {
        /// <summary>
        /// Gets or sets OrakRepository.
        /// </summary>
        /// <value>OrakRepository.</value>
        public IRepository<Orak> OrakRepository { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrakService"/> class.
        /// </summary>
        public OrakService() => OrakRepository = new OrakRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="OrakService"/> class.
        /// </summary>
        /// <param name="orak">Orak IRepo.</param>
        public OrakService(IRepository<Orak> orak) => this.OrakRepository = orak;

        /// <summary>
        /// Provides service for Orak repository => create.
        /// </summary>
        /// <param name="o">Orak object.</param>
        public void Create(Orak o) => OrakRepository.Create(o);

        /// <summary>
        /// Provides service for Orak repository => Delete.
        /// </summary>
        /// <param name="oraId">ID to be deleted.</param>
        public void Delete(int oraId) => OrakRepository.Delete(oraId);

        /// <summary>
        /// Provides service for Orak repository => GetAll.
        /// </summary>
        /// <returns>List of Orak.</returns>
        public List<Orak> GetAll() => OrakRepository.GetAll().ToList();

        /// <summary>
        /// Provides service for Orak repository => Update.
        /// </summary>
        /// <param name="o">Orak object.</param>
        public void Update(Orak o) => OrakRepository.Update(o);

        /// <summary>
        /// Provides service for Orak repository => GetOne.
        /// </summary>
        /// <param name="id">ID to be queried.</param>
        /// <returns>One record of Orak.</returns>
        public Orak GetOne(int id) => OrakRepository.GetOne(id);
    }
}
