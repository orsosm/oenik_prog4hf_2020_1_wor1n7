﻿// <copyright file="TargyakService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Public class service for Targyak.
    /// </summary>
    public class TargyakService
    {
        public IRepository<Targyak> TargyakRepository { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TargyakService"/> class.
        /// </summary>
        public TargyakService() => TargyakRepository = new TargyakRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="TargyakService"/> class.
        /// </summary>
        /// <param name="targyakRepo">Targyak repo.</param>
        public TargyakService(IRepository<Targyak> targyakRepo) => this.TargyakRepository = targyakRepo;

        /// <summary>
        /// Provides service for Targyak repository => create.
        /// </summary>
        /// <param name="t">Targyak object.</param>
        public void Create(Targyak t) => TargyakRepository.Create(t);

        /// <summary>
        /// Provides service for Targyak repository => Delete.
        /// </summary>
        /// <param name="targyId">ID to be deleted.</param>
        public void Delete(int targyId) => TargyakRepository.Delete(targyId);

        /// <summary>
        /// Provides service for Targyak repository => GetAll.
        /// </summary>
        /// <returns>List od Targyak.</returns>
        public List<Targyak> GetAll() => TargyakRepository.GetAll().ToList();

        /// <summary>
        /// Provides service for Targyak repository => Update.
        /// </summary>
        /// <param name="t">Targyak object.</param>
        public void Update(Targyak t) => TargyakRepository.Update(t);

        /// <summary>
        /// Provides service for Targyak repository => GetOne.
        /// </summary>
        /// <param name="id">Targyak ID to get.</param>
        /// <returns>Record of Targyak.</returns>
        public Targyak GetOne(int id) => TargyakRepository.GetOne(id);
    }
}
