﻿// <copyright file="ExtraFoglalkozasokTipusService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Class for ExtraFoglalkozasokService.
    /// </summary>
    public class ExtraFoglalkozasokTipusService
    {
        /// <summary>
        /// Gets or sets ExtraFoglalkozasokTipus repository.
        /// </summary>
        /// <value>
        /// ExtraFoglalkozasokTipus.
        /// </value>
        public IRepository<ExtraFoglalkozasokTipus> ExtraFoglalkozasokTipusRepository { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtraFoglalkozasokTipusService"/> class.
        /// </summary>
        public ExtraFoglalkozasokTipusService() => ExtraFoglalkozasokTipusRepository = new ExtraFoglalkozasokTipusRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtraFoglalkozasokTipusService"/> class.
        /// </summary>
        /// <param name="efTipusRepo">IRepository ExtraFoglalkozasokTipus.</param>
        public ExtraFoglalkozasokTipusService(IRepository<ExtraFoglalkozasokTipus> efTipusRepo) => this.ExtraFoglalkozasokTipusRepository = efTipusRepo;

        /// <summary>
        /// Crceates ExtraFoglalkozasokTipus record.
        /// </summary>
        /// <param name="ef">ExtraFoglalkozasokTipus object.</param>
        public void Create(ExtraFoglalkozasokTipus ef) => ExtraFoglalkozasokTipusRepository.Create(ef);

        /// <summary>
        /// Deletes one record from ExtraFoglalkozasokTipus.
        /// </summary>
        /// <param name="efID">ID of the record.</param>
        public void Delete(int efID) => ExtraFoglalkozasokTipusRepository.Delete(efID);

        /// <summary>
        /// Gets all records from ExtraFoglalkozasokTipus.
        /// </summary>
        /// <returns>List of ExtraFoglalkozasokTipus.</returns>
        public List<ExtraFoglalkozasokTipus> GetAll() => ExtraFoglalkozasokTipusRepository.GetAll().ToList();

        /// <summary>
        /// Updates a record of ExtraFoglalkozasokTipus.
        /// </summary>
        /// <param name="ef">ExtraFoglalkozasok object to be updated.</param>
        public void Update(ExtraFoglalkozasokTipus ef) => ExtraFoglalkozasokTipusRepository.Update(ef);

        /// <summary>
        /// Gets one record of ExtraFoglalkozasokTipus.
        /// </summary>
        /// <param name="id">ID of ExtraFoglalkozasok.</param>
        /// <returns>ExtraFoglalkozasok object.</returns>
        public ExtraFoglalkozasokTipus GetOne(int id) => ExtraFoglalkozasokTipusRepository.GetOne(id);
    }
}
