﻿// <copyright file="ExtraFoglalkozasokService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using MyTanoda.DA;
    using MyTanoda.RP;

    public class ExtraFoglalkozasokService
    {
        public IRepository<ExtraFoglalkozasok> ExtraFoglalkozasokRepository { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtraFoglalkozasokService"/> class.
        /// </summary>
        public ExtraFoglalkozasokService() => ExtraFoglalkozasokRepository = new ExtraFoglalkozasokRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtraFoglalkozasokService"/> class.
        /// </summary>
        /// <param name="efRepo">ExtraFoglalkozasok repo.</param>
        public ExtraFoglalkozasokService(IRepository<ExtraFoglalkozasok> efRepo) => ExtraFoglalkozasokRepository = efRepo;

        public void Create(ExtraFoglalkozasok ef) => ExtraFoglalkozasokRepository.Create(ef);

        public void Delete(int efID) => ExtraFoglalkozasokRepository.Delete(efID);

        public List<ExtraFoglalkozasok> GetAll() => ExtraFoglalkozasokRepository.GetAll().ToList();

        public void Update(ExtraFoglalkozasok ef) => ExtraFoglalkozasokRepository.Update(ef);

        public ExtraFoglalkozasok GetOne(int id) => ExtraFoglalkozasokRepository.GetOne(id);
    }
}
