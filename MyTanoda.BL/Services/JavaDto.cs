﻿// <copyright file="JavaDto.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Services
{
    /// <summary>
    /// Class for Java struct.
    /// </summary>
    public class JavaDto
    {
        /// <summary>
        /// Gets or sets Price.
        /// </summary>
        /// <value>Price.</value>
        public int Price { get; set; }

        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        /// <value>Name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="JavaDto"/> class.
        /// </summary>
        /// <param name="ar">Price to be set.</param>
        /// <param name="nev">Name to be set.</param>
        public JavaDto(int ar, string nev)
        {
            Price = ar;
            Name = nev;
        }
    }
}