﻿// <copyright file="DiakService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using MyTanoda.DA;
    using MyTanoda.DA.Interfaces;
    using MyTanoda.DA.Repositories;
    using MyTanoda.RP;

    /// <summary>
    /// DiakService Class.
    /// </summary>
    public class DiakService
    {
        /// <summary>
        /// Gets or sets DiakokRepository.
        /// </summary>
        /// <value>
        /// DiakokRepository.
        /// </value>
        public IRepository<Diakok> DiakokRepository { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DiakService"/> class.
        /// </summary>
        public DiakService() => DiakokRepository = new DiakRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="DiakService"/> class.
        /// </summary>
        /// <param name="diakokRepo">diakRepo paraneter.</param>
        public DiakService(IRepository<Diakok> diakokRepo) => this.DiakokRepository = diakokRepo;

        /// <summary>
        /// Create new Diakok entry.
        /// </summary>
        /// <param name="d">Diakok object to be created.</param>
        public void Create(Diakok d) => DiakokRepository.Create(d);

        /// <summary>
        /// Delete Diakok entry.
        /// </summary>
        /// <param name="diakokId">Diak ID to be deleted.</param>
        public void Delete(int diakokId) => DiakokRepository.Delete(diakokId);

        /// <summary>
        /// Gets all Diakok entries.
        /// </summary>
        /// <returns>List of Diakok.</returns>
        public List<Diakok> GetAll() => DiakokRepository.GetAll().ToList();

        /// <summary>
        /// Updates a Diakok entry.
        /// </summary>
        /// <param name="d">Diakok entry to be updated.</param>
        public void Update(Diakok d) => DiakokRepository.Update(d);

        /// <summary>
        /// Gets one Diakok record.
        /// </summary>
        /// <param name="id">Diakok ID to query.</param>
        /// <returns>Specific Diakok record.</returns>
        public Diakok GetOne(int id) => DiakokRepository.GetOne(id);

       
    }
}
