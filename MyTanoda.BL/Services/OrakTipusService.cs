﻿// <copyright file="OrakTipusService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Public class service for OrakTipus.
    /// </summary>
    public class OrakTipusService
    {
        /// <summary>
        /// Gets or Sets OrakTipus repository.
        /// </summary>
        /// <value>OrakTipus repo.</value>
        public IRepository<OrakTipus> OrakTipusRepository { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrakTipusService"/> class.
        /// </summary>
        public OrakTipusService() => OrakTipusRepository = new OrakTipusRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="OrakTipusService"/> class.
        /// </summary>
        /// <param name="orakrepo">Repo to initialize.</param>
        public OrakTipusService(IRepository<OrakTipus> orakrepo) => this.OrakTipusRepository = orakrepo;

        /// <summary>
        /// Provides service for OrakTipus repository => create.
        /// </summary>
        /// <param name="o">OrakTipus object.</param>
        public void Create(OrakTipus o) => OrakTipusRepository.Create(o);

        /// <summary>
        /// Provides service for OrakTipus repository => Delete.
        /// </summary>
        /// <param name="oraTipusID">ID of record to be deleted.</param>
        public void Delete(int oraTipusID) => OrakTipusRepository.Delete(oraTipusID);

        /// <summary>
        /// Provides service for OrakTipus repository => GetAll.
        /// </summary>
        /// <returns>List of OrakTipus.</returns>
        public List<OrakTipus> GetAll() => OrakTipusRepository.GetAll().ToList();

        /// <summary>
        /// Provides service for OrakTipus repository => Update.
        /// </summary>
        /// <param name="o">OrakTipus object.</param>
        public void Update(OrakTipus o) => OrakTipusRepository.Update(o);

        /// <summary>
        /// Provides service for OrakTipus repository => GetOne.
        /// </summary>
        /// <param name="id">ID of record to be queried.</param>
        /// <returns>One record of OrakTipus.</returns>
        public OrakTipus GetOne(int id) => OrakTipusRepository.GetOne(id);
    }
}
