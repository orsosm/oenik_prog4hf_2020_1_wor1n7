﻿// <copyright file="TanarokService.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using MyTanoda.DA;
    using MyTanoda.RP;

    /// <summary>
    /// Public class service for Tanarok.
    /// </summary>
    public class TanarokService
    {
        public IRepository<Tanarok> TanarokRepository { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TanarokService"/> class.
        /// </summary>
        public TanarokService() => TanarokRepository = new TanarokRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="TanarokService"/> class.
        /// </summary>
        /// <param name="tanarokRepo">Tanarok repo.</param>
        public TanarokService(IRepository<Tanarok> tanarokRepo) => this.TanarokRepository = tanarokRepo;

        /// <summary>
        /// Provides service for Tanarok repository => create.
        /// </summary>
        /// <param name="t">Tanarok object.</param>
        public void Create(Tanarok t) => TanarokRepository.Create(t);

        /// <summary>
        /// Provides service for Tanarok repository => Delete.
        /// </summary>
        /// <param name="tanarId">ID to be deleted.</param>
        public void Delete(int tanarId) => TanarokRepository.Delete(tanarId);

        /// <summary>
        /// Provides service for Tanarok repository => GetAll.
        /// </summary>
        /// <returns>List of Tanarok.</returns>
        public List<Tanarok> GetAll() => TanarokRepository.GetAll().ToList();

        /// <summary>
        /// Provides service for Tanarok repository => Update.
        /// </summary>
        /// <param name="t">Tanarok object.</param>
        public void Update(Tanarok t) => TanarokRepository.Update(t);

        /// <summary>
        /// Provides service for Tanarok repository => GetOne.
        /// </summary>
        /// <param name="id">ID to query.</param>
        /// <returns>One record of specific ID.</returns>
        public Tanarok GetOne(int id) => TanarokRepository.GetOne(id);
    }
}
