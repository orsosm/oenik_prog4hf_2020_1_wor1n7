﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyTanoda.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:50257/api/DiakApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed succesfully" : "Operation failed";
            Messenger.Default.Send(msg, "DiakResult");
        }

        public List<DiakVM> ApiGetDiak()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<DiakVM>>(json);
           // NEVER !!! SendMessage(true);

            return list;
        }

        public void ApiDelDiak(DiakVM diak)
        {
            bool success = false;
            if (diak != null)
            {
                string json = client.GetStringAsync(url + "del/" + diak.Id).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditDiak(DiakVM diak, bool isEditing)
        {
            if (diak == null) return false;
            string myURL = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing) postData.Add(nameof(DiakVM.Id), diak.Id.ToString());
            postData.Add(nameof(DiakVM.Nev), diak.Nev);
            postData.Add(nameof(DiakVM.Nem), diak.Nem);
            postData.Add(nameof(DiakVM.SzuletesiDatum), diak.SzuletesiDatum.ToString());

            string json = client.PostAsync(myURL, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);

            return (bool)obj["OperationResult"];

        }

        public void EditDiak(DiakVM diak, Func<DiakVM,bool> editor)
        {
            DiakVM clone = new DiakVM();
            if (diak != null) clone.CopyFrom(diak);

            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (diak != null) success = ApiEditDiak(clone, true);
                else success = ApiEditDiak(clone, false);
            }

            SendMessage(success == true);

        }
    }
}
