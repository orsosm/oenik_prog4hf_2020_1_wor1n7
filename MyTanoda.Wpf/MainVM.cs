﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace MyTanoda.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
		private DiakVM selectedDiak;
		private ObservableCollection<DiakVM> allDiak;

		public ObservableCollection<DiakVM> AllDiak
		{
			get { return allDiak; }
			set { Set(ref allDiak,value); }
		}


		public DiakVM SelectedDiak
		{
			get { return selectedDiak; }
			set { Set(ref selectedDiak, value); }
		}

		public ICommand AddCmd { get; private set; }
		public ICommand DelCmd { get; private set; }
		public ICommand ModCmd { get; private set; }
		public ICommand LoadCmd { get; private set; }

		public Func<DiakVM, bool> EditorFunc { get; set; }

		public MainVM()
		{
			logic = new MainLogic();

			DelCmd = new RelayCommand(() => logic.ApiDelDiak(selectedDiak));
			AddCmd = new RelayCommand(() => logic.EditDiak(null, EditorFunc));
			ModCmd = new RelayCommand(() => logic.EditDiak(selectedDiak, EditorFunc));
			LoadCmd = new RelayCommand(() => AllDiak = new ObservableCollection<DiakVM>(logic.ApiGetDiak()));
		}


	}
}
