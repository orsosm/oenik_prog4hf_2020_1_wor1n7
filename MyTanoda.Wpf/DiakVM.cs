﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTanoda.Wpf
{
    // NuGet: Newtonsoft.JSON + MvvmLightLibs
    // Startup projects: Web + Wpf
    class DiakVM : ObservableObject
    {
        private int id;
        private string nev;
        private string nem;
        private DateTime szuletesiDatum;

        public DateTime SzuletesiDatum
        {
            get { return szuletesiDatum; }
            set { Set(ref szuletesiDatum, value); }
        }

        public string Nem
        {
            get { return nem; }
            set { Set(ref nem, value); }
        }

        public  string Nev
        {
            get { return nev; }
            set { Set(ref nev, value); }
        }

        public int Id 
        {
            get { return id; }
            set { Set(ref id, value); } 
        }


        public void CopyFrom(DiakVM other)
        {
            if (other == null) return;

            this.Id = other.Id;
            this.Nev = other.Nev;
            this.Nem = other.Nem;
            this.SzuletesiDatum = other.SzuletesiDatum;
        }
    }
}
