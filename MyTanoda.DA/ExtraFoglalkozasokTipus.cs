//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyTanoda.DA
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExtraFoglalkozasokTipus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExtraFoglalkozasokTipus"/> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ExtraFoglalkozasokTipus()
        {
            this.ExtraFoglalkozasok = new HashSet<ExtraFoglalkozasok>();
        }
    
        public int id { get; set; }
        public string nev { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ExtraFoglalkozasok> ExtraFoglalkozasok { get; set; }
    }
}
