﻿// <copyright file="IDiakRepository.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.DA.Interfaces
{
    using System.Linq;

    /// <summary>
    /// Interface to provide IDiakrepository.
    /// </summary>
    public interface IDiakRepository
    {
        /// <summary>
        /// Gets all Diakok entity.
        /// </summary>
        /// <returns>Iqueryable Diakok.</returns>
        IQueryable<Diakok> GetAll();

        /// <summary>
        /// Returns one record from Diakok entities.
        /// </summary>
        /// <param name="id">ID of Diakok.</param>
        /// <returns>Diakok entity.</returns>
        Diakok GetOne(int id);

        /// <summary>
        /// Create new Diakok record.
        /// </summary>
        /// <param name="d">DIakok object.</param>
        void Create(Diakok d);

        /// <summary>
        /// UPdate Diakok .
        /// </summary>
        /// <param name="d">Diakok object.</param>
        void Update(Diakok d);

        /// <summary>
        /// Delete Diakok entity by ID.
        /// </summary>
        /// <param name="diakId">ID of Diakok to be deleted.</param>
        void Delete(int diakId);
    }
}
