﻿// <copyright file="IRepository.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.RP
{
    using System.Linq;

    /// <summary>
    /// Public Generic interface for Data layer.
    /// </summary>
    /// <typeparam name="T">Generic type of class.</typeparam>
    public interface IRepository<T>

        where T : class
    {
        /// <summary>
        /// Gets all T records.
        /// </summary>
        /// <returns>List of T.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Gets one record of T.
        /// </summary>
        /// <param name="id">ID of T.</param>
        /// <returns>One record of T.</returns>
        T GetOne(int id);

        /// <summary>
        /// Create new record of T.
        /// </summary>
        /// <param name="entity">T object.</param>
        void Create(T entity);

        /// <summary>
        /// Updates T entity.
        /// </summary>
        /// <param name="entity">T object.</param>
        void Update(T entity);

        /// <summary>
        /// Deletes a T record by ID.
        /// </summary>
        /// <param name="id">Which record to delete.</param>
        void Delete(int id);
    }
}
