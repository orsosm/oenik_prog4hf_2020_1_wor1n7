var indexSectionsWithContent =
{
  0: "acdefghijlmnopstu",
  1: "defijlmot",
  2: "m",
  3: "acdefgijlmnopt",
  4: "cdefgjostu",
  5: "adehinopst",
  6: "aclmn"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "properties",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Properties",
  6: "Pages"
};

