﻿// <copyright file="CRUDMenu.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.UI.Menu
{
    using System;
    using MyTanoda.BL.Services;
    using MyTanoda.DA;

    /// <summary>
    /// Class for CRUD Menu.
    /// </summary>
    public static class CRUDMenu
    {
        /// <summary>
        /// CRUD Menu.
        /// </summary>
        public static void SubMenu()
        {
            bool exit = false;

            do
            {
                Console.Clear();
                Console.WriteLine("CRUD menü\n\n");
                Console.WriteLine("SELECT ALL");
                Console.WriteLine(" 1 - Minden diák listázása");
                Console.WriteLine(" 2 - Minden tanár listázása");
                Console.WriteLine(" 3 - Minden tárgy listázása");
                Console.WriteLine(" 4 - Minden foglalkozás listázása");
                Console.WriteLine();
                Console.WriteLine("CREATE");
                Console.WriteLine(" 5 - Új diák Létrehozása");
                Console.WriteLine(" 6 - Új tanár létrehozása");
                Console.WriteLine(" 7 - Új tárgy létrehozása");
                Console.WriteLine(" 8 - Új foglalkozás létrehozása");
                Console.WriteLine();
                Console.WriteLine("UPDATE");
                Console.WriteLine(" 9 - Diák frissítése");
                Console.WriteLine("10 - Tanár frissítése");
                Console.WriteLine("11 - Tárgy frissítése");
                Console.WriteLine("12 - Foglalkozás frissítése");
                Console.WriteLine();
                Console.WriteLine("DELETE");
                Console.WriteLine("13 - Diák törlése");
                Console.WriteLine("14 - Tanár törlése");
                Console.WriteLine("15 - Tárgy törlése");
                Console.WriteLine("16 - Foglalkozás törlése");
                Console.WriteLine(" --------- ");

                Console.WriteLine("99 - Vissza a főmenübe");

                Console.Write("Választás: ");
                var input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                        MindenDiakListazasa();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "2":
                        MindenTanarListazasa();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "3":
                        MindenTargyListazasa();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "4":
                        MindenFoglalkozasListazasa();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "5":
                        DiakHozzaad();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "6":
                        TanarHozzaad();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "7":
                        TargyHozzaad();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "8":
                        FoglalkozasHozzaad();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "9":
                        DiakFrissites();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "10":
                        TanarFrissites();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "11":
                        TargyFrissites();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "12":
                        FoglalkozasFrissites();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "13":
                        DiakTorlese();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "14":
                        TanarTorlese();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "15":
                        TargyTorlese();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "16":
                        FoglalkozasTorlese();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "99":
                        exit = true;
                        break;
                    default:
                        Console.WriteLine("Kérlek, válassz a menüpontok közül!");
                        break;
                }
            }
            while (!exit);
        }

        /// <summary>
        /// CRUD Menu item: Lists all students.
        /// </summary>
        public static void MindenDiakListazasa()
        {
            var diakService = new DiakService();
            string nem;

            Console.WriteLine("Diákok listája: ");
            foreach (var diak in diakService.GetAll())
            {
                if (diak.nem == "f")
                {
                    nem = "fiú";
                }
                else
                {
                    nem = "lány";
                }

                Console.WriteLine($"({diak.id}) -- Név: {diak.nev}, Nem: {nem}, Születési dátum: {diak.szuletesi_datum.Value.Date.ToShortDateString()}");
            }

            Console.WriteLine("A folytatáshoz kérlek, nyomj ENTER-t!");
        }

        /// <summary>
        /// CRUD Menu item: Lists all teachers.
        /// </summary>
        public static void MindenTanarListazasa()
        {
            var tanarService = new TanarokService();
            string nem;

            Console.WriteLine("Tanárok listája");
            foreach (var tanar in tanarService.GetAll())
            {
                if (tanar.nem == "f")
                {
                    nem = "Férfi";
                }
                else
                {
                    nem = "Nő";
                }

                Console.WriteLine($"({tanar.id}) -- Név: {tanar.nev}, Nem: {nem}, Születési dátum: {tanar.szuletesi_datum.Value.Date.ToShortDateString()}");
            }

            Console.WriteLine("A folytatáshoz kérlek, nyomj ENTER-t!");
        }

        /// <summary>
        /// CRUD Menu item: Lists all subjects.
        /// </summary>
        public static void MindenTargyListazasa()
        {
            var targyService = new TargyakService();

            Console.WriteLine("Tárgyak listája");
            foreach (var targy in targyService.GetAll())
            {
                string tipus;

                if (targy.tipus == 1)
                {
                    tipus = "Általános iskola";
                }
                else
                {
                    tipus = "Középiskola";
                }

                Console.WriteLine($"({targy.id}) -- Tárgynév: {targy.nev}, Típus: {tipus}, Ár: {targy.ar} Ft");
            }

            Console.WriteLine("A folytatáshoz kérlek, nyomj egy ENTER-t!");
        }

        /// <summary>
        /// CRUD Menu item: Lists every extracurricular activity.
        /// </summary>
        public static void MindenFoglalkozasListazasa()
        {
            var foglalkozasService = new ExtraFoglalkozasokService();
            var efTipusService = new ExtraFoglalkozasokTipusService();

            Console.WriteLine("Extra Foglalkozások listája:");

            foreach (var foglalkozas in foglalkozasService.GetAll())
            {
                foreach (var eftipus in efTipusService.GetAll())
                {
                    if (eftipus.id == foglalkozas.ExtraFoglalkozasokTipus.id)
                    {
                        Console.WriteLine($"({foglalkozas.id}) -- Név: {foglalkozas.nev}, Típus: {eftipus.nev}, Időpont: {foglalkozas.datum.Value.ToShortDateString()}, Ár: {foglalkozas.ar} Ft");
                    }
                }
            }

            Console.WriteLine("A folytatáshoz kérlek, nyomj ENTER-t!");
        }

        /// <summary>
        /// CRUD menu item: Create new Diakok entry.
        /// </summary>
        public static void DiakHozzaad()
        {
            Console.WriteLine("Diák hozzáadása");
            var diakService = new DiakService();

            var uj_diak = new Diakok()
            {
                id = int.Parse(Beolvas("Mi legyen az id?")),
                nev = Beolvas("Mi legyen a diák neve?"),
                nem = Beolvas("Fiú (f) / Lány (l)?"),
                szuletesi_datum = DateTime.Parse(Beolvas("Születési dátum? (YYYY.MM.DD)")),
            };

            diakService.Create(uj_diak);

            Console.WriteLine("Létrehozás eredménye");

            var diakEll = diakService.GetOne(uj_diak.id);
            Console.WriteLine($"{diakEll.id} - {diakEll.nev} - {diakEll.nem} - {diakEll.szuletesi_datum.Value.ToShortDateString()}");

            Console.WriteLine("A folytatáshoz kérlek, nyomj ENTER-t");
        }

        /// <summary>
        /// CRUD menu item: Create new Tanarok entry.
        /// </summary>
        public static void TanarHozzaad()
        {
            Console.WriteLine("Tanár hozzáadása");
            var tanarService = new TanarokService();

            var uj_tanar = new Tanarok()
            {
                id = int.Parse(Beolvas("Mi legyen az id?")),
                nev = Beolvas("Mi legyen a tanár neve?"),
                nem = Beolvas("Férfi (f) / Nő (l)?"),
                szuletesi_datum = DateTime.Parse(Beolvas("Születési dátum? (YYYY.MM.DD)")),
            };

            tanarService.Create(uj_tanar);

            Console.WriteLine("Létrehozás eredménye");

            var tanarEll = tanarService.GetOne(uj_tanar.id);
            Console.WriteLine($"{tanarEll.id} - {tanarEll.nev} - {tanarEll.nem} - {tanarEll.szuletesi_datum.Value.ToShortDateString()}");

            Console.WriteLine("A folytatáshoz kérlek, nyomj ENTER-t");
        }

        /// <summary>
        /// CRUD menu item: Create new Targyak entry.
        /// </summary>
        public static void TargyHozzaad()
        {
            Console.WriteLine("Tárgy hozzáadása");
            var targyService = new TargyakService();

            var uj_targy = new Targyak()
            {
                id = int.Parse(Beolvas("Mi legyen az id?")),
                nev = Beolvas("Mi legyen a tanár neve?"),
                tipus = int.Parse(Beolvas("Tárgy típusa (ált isk: 1, középisk: 2)?")),
                ar = int.Parse(Beolvas("Tárgy ára:")),
            };

            targyService.Create(uj_targy);

            Console.WriteLine("Létrehozás eredménye");

            var targyEll = targyService.GetOne(uj_targy.id);
            Console.WriteLine($"{targyEll.id} - {targyEll.nev} - {targyEll.tipus} - {targyEll.ar}");

            Console.WriteLine("A folytatáshoz kérlek, nyomj ENTER-t");
        }

        /// <summary>
        /// CRUD menu item: Create new ExtraFoglalkozasok entry.
        /// </summary>
        public static void FoglalkozasHozzaad()
        {
            Console.WriteLine("Foglalkozás hozzáadása");
            var efService = new ExtraFoglalkozasokService();

            var uj_foglalkozas = new ExtraFoglalkozasok()
            {
                id = int.Parse(Beolvas("Mi legyen az id?")),
                nev = Beolvas("Mi legyen foglalkozás neve?"),
                tipus = int.Parse(Beolvas("Foglalkozás típusa (1-5)?")),
                ar = int.Parse(Beolvas("Program ára:")),
            };

            efService.Create(uj_foglalkozas);

            Console.WriteLine("Létrehozás eredménye");

            var efEll = efService.GetOne(uj_foglalkozas.id);
            Console.WriteLine($"{efEll.id} - {efEll.nev} - {efEll.tipus} - {efEll.ar}");

            Console.WriteLine("A folytatáshoz kérlek, nyomj ENTER-t");
        }

        /// <summary>
        /// CRUD menu item: Update Diakok entry.
        /// </summary>
        public static void DiakFrissites()
        {
            Console.WriteLine("Diák frissítése");
            var diakService = new DiakService();

            var diakID = int.Parse(Beolvas("Módosítandó diák ID-je?"));
            var diakAdat = diakService.GetOne(diakID);

            var uj_adatok = new Diakok()
            {
                id = diakID,
                nev = Beolvas($"Név ({diakAdat.nev})"),
                nem = Beolvas($"Nem ({diakAdat.nem})"),
                szuletesi_datum = DateTime.Parse(Beolvas($"Születési dátum ({diakAdat.szuletesi_datum.Value.ToShortDateString()})")),
            };

            diakService.Update(uj_adatok);

            var ellDiak = diakService.GetOne(diakID);
            Console.WriteLine("Frissítés eredménye:");
            Console.WriteLine($"{ellDiak.id}: {ellDiak.nev} - {ellDiak.nem} - {ellDiak.szuletesi_datum}");

            Console.WriteLine("A folytatáshoz kérlek, nyomj ENTER-t!");
        }

        /// <summary>
        /// CRUD Menu item: Update Tanarok entry.
        /// </summary>
        public static void TanarFrissites()
        {
            Console.WriteLine("Tanár frissítése");
            var tanarService = new TanarokService();

            var tanarID = int.Parse(Beolvas("Módosítandó tanár ID-je?"));
            var tanarAdat = tanarService.GetOne(tanarID);

            var uj_adatok = new Tanarok()
            {
                id = tanarID,
                nev = Beolvas($"Név ({tanarAdat.nev})"),
                nem = Beolvas($"Nem ({tanarAdat.nem})"),
                szuletesi_datum = DateTime.Parse(Beolvas($"Születési dátum ({tanarAdat.szuletesi_datum.Value.ToShortDateString()})")),
            };

            tanarService.Update(uj_adatok);

            var ellTanar = tanarService.GetOne(tanarID);
            Console.WriteLine("Frissítés eredménye:");
            Console.WriteLine($"{ellTanar.id}: {ellTanar.nev} - {ellTanar.nem} - {ellTanar.szuletesi_datum.Value.ToShortDateString()}");

            Console.WriteLine("A folytatáshoz kérlek, nyomj ENTER-t!");
        }

        /// <summary>
        /// CRUD Menu item: update Targyak entry.
        /// </summary>
        public static void TargyFrissites()
        {
            Console.WriteLine("Tárgy frissítése");
            var targyService = new TargyakService();

            var targyID = int.Parse(Beolvas("Módosítandó tárgy ID-je?"));
            var targyAdat = targyService.GetOne(targyID);

            var uj_adatok = new Targyak()
            {
                id = targyID,
                nev = Beolvas($"Név ({targyAdat.nev})"),
                tipus = int.Parse(Beolvas($"Típus (1-ált isk, 2-középisk) ({targyAdat.tipus})")),
                ar = int.Parse(Beolvas($"Ár ({targyAdat.ar} Ft)")),
            };

            targyService.Update(uj_adatok);

            var ellTargy = targyService.GetOne(targyID);
            Console.WriteLine("Frissítés eredménye:");
            Console.WriteLine($"{ellTargy.id}: {ellTargy.nev} - {ellTargy.tipus} - {ellTargy.ar} Ft");

            Console.WriteLine("A folytatáshoz kérlek, nyomj ENTER-t!");
        }

        /// <summary>
        /// CRUD Menu item: update ExtraFoglalkozasok entry.
        /// </summary>
        public static void FoglalkozasFrissites()
        {
            Console.WriteLine("Foglalkozás frissítése");
            var efService = new ExtraFoglalkozasokService();

            var efId = int.Parse(Beolvas("Módosítandó foglalkozás ID-je?"));
            var efAdat = efService.GetOne(efId);

            var uj_adatok = new ExtraFoglalkozasok()
            {
                id = efId,
                nev = Beolvas($"Név ({efAdat.nev})"),
                tipus = int.Parse(Beolvas($"Típus (1-5) ({efAdat.tipus})")),
                ar = int.Parse(Beolvas($"Ár ({efAdat.ar} Ft)")),
            };

            efService.Update(uj_adatok);

            var ellEf = efService.GetOne(efId);
            Console.WriteLine("Frissítés eredménye:");
            Console.WriteLine($"{ellEf.id}: {ellEf.nev} - {ellEf.tipus} - {ellEf.ar} Ft");

            Console.WriteLine("A folytatáshoz kérlek, nyomj ENTER-t!");
        }

        /// <summary>
        /// CRUD MENU item: Delete Diakok entry.
        /// </summary>
        public static void DiakTorlese()
        {
            Console.WriteLine("Diák Törlése");
            var diakService = new DiakService();

            var diakID = int.Parse(Beolvas("Melyik diákot töröljük?"));
            diakService.Delete(diakID);

            Console.WriteLine($"Diák {diakID} törölve, nyomj ENTER-t a folytatáshoz!");
        }

        /// <summary>
        /// CRUD Menu item: Delete Tanarok entry.
        /// </summary>
        public static void TanarTorlese()
        {
            Console.WriteLine("Tanár Törlése");
            var tanarService = new TanarokService();

            var tanarID = int.Parse(Beolvas("Melyik tanárt töröljük?"));
            tanarService.Delete(tanarID);

            Console.WriteLine($"Tanár {tanarID} törölve, nyomj ENTER-t a folytatáshoz!");
        }

        /// <summary>
        /// CRUD Menu item: Delete Targyak entry.
        /// </summary>
        public static void TargyTorlese()
        {
            Console.WriteLine("Tárgy törlése");
            var targyService = new TargyakService();

            var targyID = int.Parse(Beolvas("Melyik tárgyat töröljük?"));
            targyService.Delete(targyID);

            Console.WriteLine($"{targyID} tárgy törölve, nyomj ENTER-t a folytatáshoz!");
        }

        /// <summary>
        /// CRUD Menu: Delete ExtraFoglalkozasok entry.
        /// </summary>
        public static void FoglalkozasTorlese()
        {
            Console.WriteLine("Foglalkozás törlése");
            var efService = new ExtraFoglalkozasokService();

            var efID = int.Parse(Beolvas("Melyik foglalkozást töröljük?"));
            efService.Delete(efID);

            Console.WriteLine($"{efID} foglalkozás törölve, nyomj ENTER-t a folytatáshoz!");
        }

        /// <summary>
        /// Helper function for taking input from console with message.
        /// </summary>
        /// <param name="szoveg">Message to be displayed on console.</param>
        /// <returns>User's choice.</returns>
        private static string Beolvas(string szoveg)
        {
            Console.Write(szoveg + ": ");
            return Console.ReadLine();
        }
    }
}
