﻿// <copyright file="NonCRUDMenu.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.UI.Menu
{
    using System;
    using MyTanoda.BL.Services;
    using MyTanoda.DA;
    using MyTanoda.DA.Repositories;
    using MyTanoda.RP;

    /// <summary>
    /// Menu for Non-CRUD methods.
    /// </summary>
    public static class NonCRUDMenu
    {
        /// <summary>
        /// Submenu method.
        /// </summary>
        public static void Submenu()
        {
            bool exit = false;

            do
            {
                Console.Clear();
                Console.WriteLine("Kereszttáblás lekérdezések menü\n\n");
                Console.WriteLine(" 1 - ExtraFoglalkozáson résztvevő diákok");
                Console.WriteLine(" 2 - Adott diák hány extra foglalkozáson vesz részt?");
                Console.WriteLine(" 3 - EXtraFoglalkozásokon kísérő tanárok");
                Console.WriteLine();

                Console.WriteLine(" --------- ");

                Console.WriteLine("99 - Vissza a főmenübe");
                Console.Write("Választás: ");
                var input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        ResztvevoDiakok();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "2":
                        ExtraFoglalkozasokonResztvesz();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "3":
                        ExtraFoglalkozasokonKisero();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "99":
                        exit = true;
                        break;
                    default:
                        Console.WriteLine("Kérlek, válassz a menüpontok közül!");
                        break;
                }
            }
            while (!exit);
        }

        /// <summary>
        /// Submenu item: Lists the perticipants of ExtraFoglalkozasok.
        /// </summary>
        public static void ResztvevoDiakok()
        {
            var diakRepo = new DiakRepository();
            var tanarRepo = new TanarokRepository();
            var efRepo = new ExtraFoglalkozasokRepository();
            var efTipRepo = new ExtraFoglalkozasokTipusRepository();
            var resztvRepo = new ExtraResztvevokRepository();
            var foglId = int.Parse(Beolvas("Melyik foglalkozás résztvevőit listázzam (1-8)?"));
            int sorszam = 0;

            FoglalkozasokServices foglServ = new FoglalkozasokServices(diakRepo, tanarRepo, efRepo, efTipRepo, resztvRepo);

            foreach (var resztv in foglServ.ExtraFoglalkozasokResztvevoDiakok(foglId))
            {
                sorszam++;
                Console.WriteLine($"{sorszam} -> {resztv}");
            }

            Console.WriteLine("A folytatáshoz kérlek, nyomj ENTER-t!");
        }

        /// <summary>
        /// Submenu item: Lists the number of ExtraFoglalkozasok where diakID is participant.
        /// </summary>
        public static void ExtraFoglalkozasokonResztvesz()
        {
            var diakRepo = new DiakRepository();
            var tanarRepo = new TanarokRepository();
            var efRepo = new ExtraFoglalkozasokRepository();
            var efTipRepo = new ExtraFoglalkozasokTipusRepository();
            var resztvRepo = new ExtraResztvevokRepository();
            var diakId = int.Parse(Beolvas("Melyik diák foglalkozás számára kíváncsi? ID?"));

            FoglalkozasokServices foglServ = new FoglalkozasokServices(diakRepo, tanarRepo, efRepo, efTipRepo, resztvRepo);

            Console.WriteLine($"{diakId}. azonosítójú diák {foglServ.ExtraFoglalkozasokonResztVeszSzam(diakId)} db extra foglalkozáson vesz részt.");

            Console.WriteLine("A folytatáshoz kérlek, nyomj ENTER-t!");
        }

        /// <summary>
        /// Submenu item: Lists Tanarok as accompanyiers on ExtraFoglalkozasok.
        /// </summary>
        public static void ExtraFoglalkozasokonKisero()
        {
            var diakRepo = new DiakRepository();
            var tanarRepo = new TanarokRepository();
            var efRepo = new ExtraFoglalkozasokRepository();
            var efTipRepo = new ExtraFoglalkozasokTipusRepository();
            var resztvRepo = new ExtraResztvevokRepository();

            FoglalkozasokServices foglServ = new FoglalkozasokServices(diakRepo, tanarRepo, efRepo, efTipRepo, resztvRepo);

            foreach (var item in foglServ.ExtraFoglalkozasokKiseroTanarok())
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("A folytatáshoz kérlek, nyomj ENTER-t!");
        }

        /// <summary>
        /// Helper function for taking input from console with message.
        /// </summary>
        /// <param name="szoveg">Message to be displayed on console.</param>
        /// <returns>User's choice.</returns>
        private static string Beolvas(string szoveg)
        {
            Console.Write(szoveg + ": ");
            return Console.ReadLine();
        }
    }
}
