﻿// <copyright file="MainMenuPage.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.UI.Menu
{
    using System;
    using MyTanoda.BL.Services;
    using MyTanoda.DA.Repositories;
    using MyTanoda.RP;

    public static class MainMenuPage
    {
        /// <summary>
        /// Main menu.
        /// </summary>
        public static void SimpleMenu()
        {
            bool exit = false;
            do
            {
                
                Console.Clear();
                Console.WriteLine("MENÜ\n\n");
                Console.WriteLine("Kérlek, válassz a menüpontok közül!\n");
                Console.WriteLine($"1 - CRUD műveletek");
                Console.WriteLine($"2 - Kereszttáblás lekérdezések");
                Console.WriteLine($"3 - Java végpont");
                Console.WriteLine($"9 - Kilépés");
                Console.Write("Választás: ");
                var input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                        CRUDMenu.SubMenu();
                        break;
                    case "2":
                        NonCRUDMenu.Submenu();
                        break;
                    case "3":
                        JavaVegpont();
                        Console.ReadLine();
                        break;
                    case "9":
                        exit = true;
                        break;
                    default:
                        break;
                }
            }
            while (!exit);
        }

        /// <summary>
        /// Request random Foglalkozas from Java endpoint.
        /// </summary>
        public static void JavaVegpont()
        {
            var diakRepo = new DiakRepository();
            var tanarRepo = new TanarokRepository();
            var efRepo = new ExtraFoglalkozasokRepository();
            var efTipRepo = new ExtraFoglalkozasokTipusRepository();
            var resztvRepo = new ExtraResztvevokRepository();

            Console.WriteLine("Hány foglalkozásra kérjek ajánlatot?");
            var info = Console.ReadLine();
            var ar = 0;

            FoglalkozasokServices foglServ = new FoglalkozasokServices(diakRepo, tanarRepo, efRepo, efTipRepo, resztvRepo);

            foreach (var item in foglServ.GetExtraFoglalkozasokFromJavaEndpoint(info))
            {
                Console.WriteLine($"Név: {item.Name} - Ár: {item.Price} Ft");
                ar += item.Price;
            }
            Console.WriteLine($"Összesen: {ar} Ft.");
        }
    }
}
