﻿// <copyright file="Program.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.UI
{
    using MyTanoda.UI.Menu;

    /// <summary>
    /// Main entry point. Starts with MainMenu.
    /// </summary>
    internal static class Program
    {
        private static void Main()
        {
           MainMenuPage.SimpleMenu();
        }
    }
}
