﻿using AutoMapper;
using MyTanoda.BL.Interfaces;
using MyTanoda.BL.Services;
using MyTanoda.DA;
using MyTanoda.DA.Repositories;
using MyTanoda.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyTanoda.Web.Controllers
{
    public class DiakApiController : ApiController
    {
        public class ApiResult // SINGLE POINT OF USAGE!
        {
            public bool OperationResult { get; set; }
        }

        IDiakService service;
        IMapper mapper;
        DiakService ds;
        DiakokViewModel vm;


        public DiakApiController()
        {
            MyTanodaDBEntities ctx = new MyTanodaDBEntities();
            DiakRepository diakRepo = new DiakRepository(ctx);
            ds = new DiakService(diakRepo);

            mapper = MapperFactory.CreateMapper();
            //ds = new DiakService(diakRepo);
            vm = new DiakokViewModel();
            vm.EditedDiak = new Diak();

        }

        // GET api/DiakApi
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Diak> GetAll()
        {
           
            var diakok = ds.GetAll();
            return mapper.Map<IList<Diakok>, List<Models.Diak>>(diakok);
        }

        // GET api/CarsApi/del/27
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneDiak(int id)
        {
            bool success = true;
            try
            {
                ds.Delete(id);
            }
            catch (Exception)
            {
                success = false;
            }
            return new ApiResult() { OperationResult = success };
        }

        // POST api/CarsApi/add + diak
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneDiak(Diak diak)
        {
            //var lastID = ds.GetAll()
            //            .OrderBy(x => x.id)
            //            .Reverse()
            //            .Select(x => x.id)
            //            .Take(1);

            //int new_id = Convert.ToInt32(lastID) + 1;

            var uj_adatok = new Diakok()
            {
                id = diak.Id,
                nev = diak.Nev,
                nem = diak.Nem,
                szuletesi_datum = diak.SzuletesiDatum,
            };
            ds.Create(uj_adatok);

            return new ApiResult() { OperationResult = true };
        }

        // GET api/CarsApi/del/27
        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneDiak(Diak diak)
        {
            bool success = true;

            try
            {
                var uj_adatok = new Diakok()
                {
                    id = diak.Id,
                    nev = diak.Nev,
                    nem = diak.Nem,
                    szuletesi_datum = diak.SzuletesiDatum,
                };
                ds.Update(uj_adatok);
            }
            catch
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }
    }
}
