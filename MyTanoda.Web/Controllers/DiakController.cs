﻿using AutoMapper;
using MyTanoda.BL.Services;
using MyTanoda.DA;
using MyTanoda.DA.Interfaces;
using MyTanoda.DA.Repositories;
using MyTanoda.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyTanoda.Web.Controllers
{
    public class DiakController : Controller
    {
        DiakService ds;
        IMapper mapper;
        DiakokViewModel vm;

        public DiakController()
        {
            MyTanodaDBEntities ctx = new MyTanodaDBEntities();
            DiakRepository diakRepo = new DiakRepository(ctx);
            ds = new DiakService(diakRepo);

            mapper = MapperFactory.CreateMapper();
            vm = new DiakokViewModel();
            vm.EditedDiak = new Diak();
            var diakok = ds.GetAll();
            vm.ListofDiakok = mapper.Map<IList<Diakok>, List<Models.Diak>>(diakok);
            

        }

        private Diak GetDiak(int id)
        {
            var egyDiak = ds.GetOne(id);
            return mapper.Map<Diakok,Diak>(egyDiak);

        }

        // GET: Diak
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("DiakIndex",vm);
        }

        // GET: Diak/Details/5
        public ActionResult Details(int id)
        {
            return View("DiakDetails", GetDiak(id));
        }

        // 1x Delete, 1x Edit+GET, 1x Edit+Post

        // GET: /Cars/Remove/5
        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete Fail";
            try
            {
                ds.Delete(id);
            }
            catch (Exception)
            {
                TempData["editResult"] = "Delete failed";
            }
            return RedirectToAction(nameof(Index));
        }
        // GET /Cars/Edit/5
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedDiak = GetDiak(id);
            return View("DiakIndex", vm);
        }

        // POST /Cars/Edit
        [HttpPost]
        public ActionResult Edit(Diak diak, string editAction)
        {
            if (ModelState.IsValid && diak != null)
            {
                TempData["editResult"] = "Delete OK!";
                if (editAction == "AddNew")
                {
                    var lastID = ds.GetAll()
                        .OrderBy(x => x.id)
                        .Reverse()
                        .Select(x => x.id)
                        .Take(1);

                    int new_id = Convert.ToInt32(lastID) + 1;

                    var uj_adatok = new Diakok()
                    {
                        id = new_id,
                        nev = diak.Nev,
                        nem = diak.Nem,
                        szuletesi_datum = diak.SzuletesiDatum,
                    };
                    ds.Create(uj_adatok);
                } else
                {
                    try
                    {
                        var uj_adatok = new Diakok()
                        {
                            id = diak.Id,
                            nev = diak.Nev,
                            nem = diak.Nem,
                            szuletesi_datum = diak.SzuletesiDatum,
                        };
                        ds.Update(uj_adatok);
                    }
                    catch
                    {
                        TempData["editResult"] = "EDIT Failed!";
                    }
                }
            }
            else 
            {
                ViewData["editAction"] = "Edit";
                vm.EditedDiak = diak;
                return View("DiakIndex", vm);
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
