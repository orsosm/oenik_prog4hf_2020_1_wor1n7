﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyTanoda.Web.Models
{
    public class DiakokViewModel
    {
        public Diak EditedDiak { get; set; }
        public List<Diak> ListofDiakok { get; set; }
    }
}