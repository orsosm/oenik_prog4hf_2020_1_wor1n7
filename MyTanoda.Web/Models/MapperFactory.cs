﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyTanoda.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MyTanoda.DA.Diakok, MyTanoda.Web.Models.Diak>().
                ForMember(dest => dest.Id, map => map.MapFrom(src => src.id)).
                ForMember(dest => dest.Nev, map => map.MapFrom(src => src.nev)).
                ForMember(dest => dest.Nem, map => map.MapFrom(src => src.nem)).
                ForMember(dest => dest.SzuletesiDatum, map => map.MapFrom(src => src.szuletesi_datum));
            });

            return config.CreateMapper();
        }
    }
}