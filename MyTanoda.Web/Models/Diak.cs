﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyTanoda.Web.Models
{
    // Form Model
    public class Diak
    {
        [Display(Name = "Diák ID")]
        [Required]
        public int Id { get; set; }
        
        [Display(Name = "Diák Neve")]
        [Required]
        public string Nev { get; set; }

        [Display(Name = "Diák NeMe")]
        [Required]
        public string Nem { get; set; }

        [Display(Name = "Diák születési dátuma")]
        [Required]
        public DateTime SzuletesiDatum { get; set; }
    }
}