﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyTanoda.ConsoleClient
{
    // TODO: NSwag / SwashBuckle
    // API Testing: Selenium, Postman
    // NuGet: Newtonsoft.JSON
    // TODO: muliple startup projects
    public class Diak
    {
        public int Id { get; set; }
        public string Nev { get; set; }
        public string Nem { get; set; }
        public DateTime SzuletesiDatum { get; set; }
        public override string ToString()
        {
            return $"ID={Id}\tNév={Nev}\t\t\tNem={Nem}\tSzületési dátum={SzuletesiDatum}";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("WAITING.....");
            Console.ReadLine();

            string url = "http://localhost:50257/api/DiakApi/";
            // No error handling...
            // WebClient => HttpClient

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Diak>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item.ToString());
                }
                Console.WriteLine();
                // WebClient => NameValueCollection postData + byte[] resposnseBytes

                Dictionary<string, string> postData;
                string response;

                #region Add new Diak
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Diak.Id), "101");
                postData.Add(nameof(Diak.Nev), "Teszt Elek");
                postData.Add(nameof(Diak.Nem), "f");
                postData.Add(nameof(Diak.SzuletesiDatum), "1999.12.16");

                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData))
                    .Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);

                #endregion

                #region Mod Diak
                int diakID = JsonConvert.DeserializeObject<List<Diak>>(json).Single(x => x.Nev == "Teszt Elek").Id;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Diak.Id), diakID.ToString());
                postData.Add(nameof(Diak.Nev), "Teszt Elek");
                postData.Add(nameof(Diak.Nem), "f");
                postData.Add(nameof(Diak.SzuletesiDatum), "1966.12.18");


                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData))
                    .Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);

                #endregion

                #region Del Diak
                response = client.GetStringAsync(url + "del/" + diakID).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                #endregion
            }



            Console.WriteLine("PROGRAM VÉGE, ÜSS ENTERT A KILÉPÉSHEZ!");
            Console.ReadLine();
        }
    }
}
