﻿// <copyright file="LogicTest.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.BL.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using MyTanoda.BL.Services;
    using MyTanoda.DA;
    using MyTanoda.DA.Interfaces;
    using MyTanoda.DA.Repositories;
    using MyTanoda.RP;
    using NUnit.Framework;

    /// <summary>
    /// Unit tests.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private static Mock<IRepository<Diakok>> diakokMock;
        private static Mock<IRepository<ExtraFoglalkozasok>> extraFoglalkozasokMock;
        private static Mock<IRepository<ExtraFoglalkozasokTipus>> extraFoglalkozasokTipusMock;
        private static Mock<IRepository<ExtraResztvevok>> extraResztvevokMock;
        private static Mock<IRepository<Tanarok>> tanarokMock;

        private DiakService diakService;
        private ExtraFoglalkozasokService extraFoglalkozasokService;
        private ExtraFoglalkozasokTipusService extraFoglalkozasokTipusService;
        private ExtraResztvevokService extraResztvevokService;
        private TanarokService tanarokService;

        /// <summary>
        /// Setting up environment for testing.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            // Setting up DiakokMock
            diakokMock = new Mock<IRepository<Diakok>>();
            var diakokMockList = new List<Diakok>
            {
                new Diakok { id = 1, nev = "Teszt Elek ", nem = "f", szuletesi_datum = System.DateTime.Parse("2011-12-16") },
                new Diakok { id = 2, nev = "Teszt Manci", nem = "l", szuletesi_datum = System.DateTime.Parse("2012-12-16") },
                new Diakok { id = 3, nev = "Teszt Józsi", nem = "f", szuletesi_datum = System.DateTime.Parse("2013-12-16") },
                new Diakok { id = 4, nev = "Teszt Panka", nem = "l", szuletesi_datum = System.DateTime.Parse("2014-12-16") },
            };
            diakokMock.Setup(d => d.GetAll()).Returns(diakokMockList.AsQueryable);

            // Setting up tanarokMock
            tanarokMock = new Mock<IRepository<Tanarok>>();
            var tanarokMockList = new List<Tanarok>
            {
                new Tanarok { id = 1, nev = "Teszt Tanar", nem = "f", szuletesi_datum = System.DateTime.Parse("1979-03-01") },
                new Tanarok { id = 2, nev = "Teszt Tanar2", nem = "l", szuletesi_datum = System.DateTime.Parse("1984-03-01") },
            };
            tanarokMock.Setup(t => t.GetAll()).Returns(tanarokMockList.AsQueryable);

            // Setting up extraFoglalkozasokMock.
            extraFoglalkozasokMock = new Mock<IRepository<ExtraFoglalkozasok>>();
            var extraFoglalkozasokMockList = new List<ExtraFoglalkozasok>
            {
                new ExtraFoglalkozasok { id = 1, nev = "Extra Foglalkozas 1", tipus = 1, datum = System.DateTime.Parse("2020.01.11"), ar = 1500 },
                new ExtraFoglalkozasok { id = 2, nev = "Extra Foglalkozas 2", tipus = 2, datum = System.DateTime.Parse("2020.01.12"), ar = 2500 },
                new ExtraFoglalkozasok { id = 3, nev = "Extra Foglalkozas 3", tipus = 1, datum = System.DateTime.Parse("2020.01.14"), ar = 3500 },
                new ExtraFoglalkozasok { id = 4, nev = "Extra Foglalkozas 4", tipus = 3, datum = System.DateTime.Parse("2020.01.15"), ar = 4500 },
            };
            extraFoglalkozasokMock.Setup(x => x.GetAll()).Returns(extraFoglalkozasokMockList.AsQueryable);

            // Setting up extraFoglalkozasokTipusMock
            extraFoglalkozasokTipusMock = new Mock<IRepository<ExtraFoglalkozasokTipus>>();
            var extraFoglalkozasokTipusMockList = new List<ExtraFoglalkozasokTipus>
            {
                new ExtraFoglalkozasokTipus { id = 1, nev = "Kirándulás" },
                new ExtraFoglalkozasokTipus { id = 2, nev = "Koncert" },
                new ExtraFoglalkozasokTipus { id = 3, nev = "Múzeumlátogatás" },
            };
            extraFoglalkozasokTipusMock.Setup(x => x.GetAll()).Returns(extraFoglalkozasokTipusMockList.AsQueryable);

            // Setting up ExtraResztvevokService
            extraResztvevokMock = new Mock<IRepository<ExtraResztvevok>>();
            var extraResztvevokMockList = new List<ExtraResztvevok>
            {
                new ExtraResztvevok { id = 1, diak_id = 1, extra_id = 1, tanar_id = 1 },
                new ExtraResztvevok { id = 2, diak_id = 1, extra_id = 2, tanar_id = 2 },
                new ExtraResztvevok { id = 3, diak_id = 1, extra_id = 3, tanar_id = 1 },
                new ExtraResztvevok { id = 4, diak_id = 2, extra_id = 1, tanar_id = 1 },
                new ExtraResztvevok { id = 5, diak_id = 2, extra_id = 2, tanar_id = 2 },
                new ExtraResztvevok { id = 6, diak_id = 2, extra_id = 3, tanar_id = 1 },
                new ExtraResztvevok { id = 7, diak_id = 3, extra_id = 1, tanar_id = 1 },
            };
            extraResztvevokMock.Setup(x => x.GetAll()).Returns(extraResztvevokMockList.AsQueryable);

            // Initializing Services
            this.diakService = new DiakService(diakokMock.Object);
            this.tanarokService = new TanarokService(tanarokMock.Object);
            this.extraFoglalkozasokService = new ExtraFoglalkozasokService(extraFoglalkozasokMock.Object);
            this.extraFoglalkozasokTipusService = new ExtraFoglalkozasokTipusService(extraFoglalkozasokTipusMock.Object);
            this.extraResztvevokService = new ExtraResztvevokService(extraResztvevokMock.Object);
        }

        /// <summary>
        /// Tests GetAll().
        /// </summary>
        [Test]
        public void Test_WhenGetAllCalledThrowsNotException()
        {
            this.diakService.GetAll();
            diakokMock.Verify(x => x.GetAll(), Times.Once);

            this.tanarokService.GetAll();
            tanarokMock.Verify(x => x.GetAll(), Times.Once);

            this.extraFoglalkozasokService.GetAll();
            extraFoglalkozasokMock.Verify(x => x.GetAll(), Times.Once);

            this.extraFoglalkozasokTipusService.GetAll();
            extraFoglalkozasokTipusMock.Verify(x => x.GetAll(), Times.Once);

            this.extraResztvevokService.GetAll();
            extraResztvevokMock.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests for GetOne(id).
        /// </summary>
        [Test]
        public void Test_WhenGetOneCalledThrowsNotException()
        {
            this.diakService.GetOne(1);
            diakokMock.Verify(x => x.GetOne(1), Times.Once);

            this.tanarokService.GetOne(1);
            tanarokMock.Verify(x => x.GetOne(1), Times.Once);

            this.extraFoglalkozasokService.GetOne(1);
            extraFoglalkozasokMock.Verify(x => x.GetOne(1), Times.Once);

            this.extraFoglalkozasokTipusService.GetOne(1);
            extraFoglalkozasokTipusMock.Verify(x => x.GetOne(1), Times.Once);

            this.extraResztvevokService.GetOne(1);
            extraResztvevokMock.Verify(x => x.GetOne(1), Times.Once);
        }

        /// <summary>
        /// Test for Create().
        /// </summary>
        [Test]
        public void Test_WhenCreateCalledThrowsNotException()
        {
            this.diakService.Create(It.IsAny<Diakok>());
            diakokMock.Verify(x => x.Create(It.IsAny<Diakok>()), Times.Once);

            this.tanarokService.Create(It.IsAny<Tanarok>());
            tanarokMock.Verify(x => x.Create(It.IsAny<Tanarok>()));

            this.extraFoglalkozasokService.Create(It.IsAny<ExtraFoglalkozasok>());
            extraFoglalkozasokMock.Verify(x => x.Create(It.IsAny<ExtraFoglalkozasok>()));

            this.extraFoglalkozasokTipusService.Create(It.IsAny<ExtraFoglalkozasokTipus>());
            extraFoglalkozasokTipusMock.Verify(x => x.Create(It.IsAny<ExtraFoglalkozasokTipus>()));

            this.extraResztvevokService.Create(It.IsAny<ExtraResztvevok>());
            extraResztvevokMock.Verify(x => x.Create(It.IsAny<ExtraResztvevok>()));
        }

        /// <summary>
        /// Test for Update().
        /// </summary>
        [Test]
        public void Test_WhenUpdateCalledThrowsNotException()
        {
            this.diakService.Update(It.IsAny<Diakok>());
            diakokMock.Verify(x => x.Update(It.IsAny<Diakok>()), Times.Once);

            this.tanarokService.Update(It.IsAny<Tanarok>());
            tanarokMock.Verify(x => x.Update(It.IsAny<Tanarok>()));

            this.extraFoglalkozasokService.Update(It.IsAny<ExtraFoglalkozasok>());
            extraFoglalkozasokMock.Verify(x => x.Update(It.IsAny<ExtraFoglalkozasok>()));

            this.extraFoglalkozasokTipusService.Update(It.IsAny<ExtraFoglalkozasokTipus>());
            extraFoglalkozasokTipusMock.Verify(x => x.Update(It.IsAny<ExtraFoglalkozasokTipus>()));

            this.extraResztvevokService.Update(It.IsAny<ExtraResztvevok>());
            extraResztvevokMock.Verify(x => x.Update(It.IsAny<ExtraResztvevok>()));
        }

        /// <summary>
        /// Tests for Delete(id).
        /// </summary>
        [Test]
        public void Test_WhenDeleteCalledThrowsNotException()
        {
            this.diakService.Delete(1);
            diakokMock.Verify(x => x.Delete(1), Times.Once);

            this.tanarokService.Delete(1);
            tanarokMock.Verify(x => x.Delete(1), Times.Once);

            this.extraFoglalkozasokService.Delete(1);
            extraFoglalkozasokMock.Verify(x => x.Delete(1), Times.Once);

            this.extraFoglalkozasokTipusService.Delete(1);
            extraFoglalkozasokTipusMock.Verify(x => x.Delete(1), Times.Once);

            this.extraResztvevokService.Delete(1);
            extraResztvevokMock.Verify(x => x.Delete(1), Times.Once);
        }

        /// <summary>
        /// Testing FoglalkozasokService => ExtraFoglalkozasokResztvevoDiakok.
        /// </summary>
        [Test]
        public void Test_GetFoglalkozasDiakokListaReturnsGoodResult()
        {
            var efogl = new FoglalkozasokServices(
                diakokMock.Object,
                tanarokMock.Object,
                extraFoglalkozasokMock.Object,
                extraFoglalkozasokTipusMock.Object,
                extraResztvevokMock.Object);

            var result = efogl.ExtraFoglalkozasokResztvevoDiakok(1);

            Assert.That(result.Count == 3);
            extraFoglalkozasokMock.Verify(x => x.GetAll(), Times.Once);
            diakokMock.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// TEsting FoglalkozasokService => ExtraFoglalkozasokonResztVeszSzam.
        /// </summary>
        [Test]
        public void Test_GetDiakFoglalkozasokSzamaREturnsInteger()
        {
            var efogl = new FoglalkozasokServices(
             diakokMock.Object,
             tanarokMock.Object,
             extraFoglalkozasokMock.Object,
             extraFoglalkozasokTipusMock.Object,
             extraResztvevokMock.Object);

            var result = efogl.ExtraFoglalkozasokonResztVeszSzam(1);

            Assert.That(result == 3);

            diakokMock.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Testing FoglalkozasokService => ExtraFoglalkozasokKiseroTanarok.
        /// </summary>
        [Test]
        public void Test_GetTanarKiserok()
        {
            var efogl = new FoglalkozasokServices(
             diakokMock.Object,
             tanarokMock.Object,
             extraFoglalkozasokMock.Object,
             extraFoglalkozasokTipusMock.Object,
             extraResztvevokMock.Object);

            var result = efogl.ExtraFoglalkozasokKiseroTanarok();

            Assert.That(result.Count == 3);

            tanarokMock.Verify(x => x.GetAll(), Times.Once);
        }
    }
}
