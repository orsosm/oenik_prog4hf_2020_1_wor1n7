﻿// <copyright file="OrakTipusRepository.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.RP
{
    using System;
    using System.Linq;
    using MyTanoda.DA;

    /// <summary>
    /// Public class for OrakTipus repository.
    /// </summary>
    public class OrakTipusRepository : IRepository<OrakTipus>
    {
        private readonly MyTanodaDBEntities ctx = new MyTanodaDBEntities();

        public void Create(OrakTipus entity)
        {
            ctx.OrakTipus.Add(entity);
            ctx.SaveChanges();
        }

        public void Delete(int id)
        {
            OrakTipus ora = ctx.OrakTipus.Where(d => d.id == id).First();
            ctx.OrakTipus.Remove(ora);

            try
            {
                ctx.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception("Adatbázis hiba");
            }
        }

        public IQueryable<OrakTipus> GetAll() => ctx.Set<OrakTipus>();

        public OrakTipus GetOne(int id) => GetAll().SingleOrDefault(x => x.id == id);

        public void Update(OrakTipus entity)
        {
            var akt_oraTipus = ctx.OrakTipus.First(x => x.id == entity.id);
            akt_oraTipus.id = entity.id;
            akt_oraTipus.nev = entity.nev;

            ctx.SaveChanges();
        }
    }
}
