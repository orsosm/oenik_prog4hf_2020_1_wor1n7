﻿// <copyright file="TanarokRepository.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.RP
{
    using System;
    using System.Linq;
    using MyTanoda.DA;

    /// <summary>
    /// Public class for Tanarok repository.
    /// </summary>
    public class TanarokRepository : IRepository<Tanarok>
    {
        private readonly MyTanodaDBEntities ctx = new MyTanodaDBEntities();

        public void Create(Tanarok entity)
        {
            ctx.Tanarok.Add(entity);
            ctx.SaveChanges();
        }

        public void Delete(int id)
        {
            Tanarok tanar = ctx.Tanarok.Where(d => d.id == id).First();
            ctx.Tanarok.Remove(tanar);

            try
            {
                ctx.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception("Adatbázis hiba");
            }
        }

        public IQueryable<Tanarok> GetAll() => ctx.Set<Tanarok>();

        public Tanarok GetOne(int id) => GetAll().SingleOrDefault(x => x.id == id);

        public void Update(Tanarok entity)
        {
            var akt_tanar = ctx.Tanarok.First(x => x.id == entity.id);
            akt_tanar.id = entity.id;
            akt_tanar.nev = entity.nev;
            akt_tanar.nem = entity.nem;
            akt_tanar.szuletesi_datum = entity.szuletesi_datum;

            ctx.SaveChanges();
        }
    }
}
