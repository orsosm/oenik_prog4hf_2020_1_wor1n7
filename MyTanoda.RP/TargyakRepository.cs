﻿// <copyright file="TargyakRepository.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.RP
{
    using System;
    using System.Linq;
    using MyTanoda.DA;

    /// <summary>
    /// Public class for Targyak repository.
    /// </summary>
    public class TargyakRepository : IRepository<Targyak>
    {
        private readonly MyTanodaDBEntities ctx = new MyTanodaDBEntities();

        public void Create(Targyak entity)
        {
            ctx.Targyak.Add(entity);
            ctx.SaveChanges();
        }

        public void Delete(int id)
        {
            Targyak targy = ctx.Targyak.Where(d => d.id == id).First();
            ctx.Targyak.Remove(targy);

            try
            {
                ctx.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception("Adatbázis hiba");
            }
        }

        public IQueryable<Targyak> GetAll() => ctx.Set<Targyak>();

        public Targyak GetOne(int id) => GetAll().SingleOrDefault(x => x.id == id);

        public void Update(Targyak entity)
        {
            var akt_targy = ctx.Targyak.First(x => x.id == entity.id);
            akt_targy.id = entity.id;
            akt_targy.nev = entity.nev;
            akt_targy.tipus = entity.tipus;
            akt_targy.ar = entity.ar;

            ctx.SaveChanges();
        }
    }
}
