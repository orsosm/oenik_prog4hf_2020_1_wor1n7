﻿// <copyright file="DIakRepository.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.DA.Repositories
{
    using System;
    using System.Linq;
    using MyTanoda.DA.Interfaces;
    using MyTanoda.RP;

    /// <summary>
    /// Public class for Diak repository.
    /// </summary>
    public class DiakRepository : IRepository<Diakok>
    {
        private readonly MyTanodaDBEntities ctx = new MyTanodaDBEntities();

        /// <summary>
        /// Creates Diakok entry.
        /// </summary>
        /// <param name="d">Diakok entity.</param>
        public void Create(Diakok d)
        {
            ctx.Diakok.Add(d);
            ctx.SaveChanges();
        }

        /// <summary>
        /// Deletes an existing Diakik entry.
        /// </summary>
        /// <param name="diakId">ID of Diakok.</param>
        public void Delete(int diakId)
        {
            Diakok diak = ctx.Diakok.Where(d => d.id == diakId).First();
            ctx.Diakok.Remove(diak);

            try
            {
               ctx.SaveChanges();
            }
            catch (Exception)
            {
               throw new Exception("Adatbázis hiba");
            }
        }



        /// <summary>
        ///  Returns all records from table diak.
        /// </summary>
        /// <returns>
        /// IQueryable of diak table.
        /// </returns>
        public IQueryable<Diakok> GetAll() => ctx.Set<Diakok>();

        /// <summary>
        /// Returns One record from table diak by ID.
        /// </summary>
        /// <param name="id">The id of the student we are looking for.</param>
        /// <returns> An instance of diak if found.</returns>
        public Diakok GetOne(int id) => GetAll().SingleOrDefault(x => x.id == id);

        /// <summary>
        /// Updates a Diakok entry.
        /// </summary>
        /// <param name="d">Diakok entity to be updated.</param>
        public void Update(Diakok d)
        {
            var akt_diak = ctx.Diakok.First(x => x.id == d.id);
            akt_diak.nev = d.nev;
            akt_diak.nem = d.nem;
            akt_diak.szuletesi_datum = d.szuletesi_datum;
            ctx.SaveChanges();
        }

        public DiakRepository()
        {

        }

        public DiakRepository(MyTanodaDBEntities ctx)
        {
            this.ctx = ctx;
        }
    }
}
