﻿// <copyright file="ExtraFoglalkozasokRepository.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.RP
{
    using System;
    using System.Linq;
    using MyTanoda.DA;

    /// <summary>
    /// Public class for ExtraFoglalkozasok repository.
    /// </summary>
    public class ExtraFoglalkozasokRepository : IRepository<ExtraFoglalkozasok>
    {
        private readonly MyTanodaDBEntities ctx = new MyTanodaDBEntities();

        public void Create(ExtraFoglalkozasok entity)
        {
            ctx.ExtraFoglalkozasok.Add(entity);
            ctx.SaveChanges();
        }

        public void Delete(int id)
        {
            ExtraFoglalkozasok ef = ctx.ExtraFoglalkozasok.Where(d => d.id == id).First();
            ctx.ExtraFoglalkozasok.Remove(ef);

            try
            {
               ctx.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception("Adatbázis hiba");
            }
        }

        public IQueryable<ExtraFoglalkozasok> GetAll() => ctx.Set<ExtraFoglalkozasok>();

        public ExtraFoglalkozasok GetOne(int id) => GetAll().SingleOrDefault(x => x.id == id);

        public void Update(ExtraFoglalkozasok entity)
        {
            var akt_ef = ctx.ExtraFoglalkozasok.First(x => x.id == entity.id);
            akt_ef.id = entity.id;
            akt_ef.nev = entity.nev;
            akt_ef.tipus = entity.tipus;
            akt_ef.ar = entity.ar;
            akt_ef.datum = entity.datum;

            ctx.SaveChanges();
        }
    }
}
