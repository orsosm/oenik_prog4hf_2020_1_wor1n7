﻿// <copyright file="TargyTipusRepository.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.RP
{
    using System;
    using System.Linq;
    using MyTanoda.DA;

    /// <summary>
    /// Public class fpor TargyTipus Repository.
    /// </summary>
    public class TargyTipusRepository : IRepository<TargyTipus>
    {
        private readonly MyTanodaDBEntities ctx = new MyTanodaDBEntities();

        public void Create(TargyTipus entity)
        {
            ctx.TargyTipus.Add(entity);
            ctx.SaveChanges();
        }

        public void Delete(int id)
        {
            TargyTipus tt = ctx.TargyTipus.Where(d => d.id == id).First();
            ctx.TargyTipus.Remove(tt);

            try
            {
                ctx.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception("Adatbázis hiba");
            }
        }

        public IQueryable<TargyTipus> GetAll() => ctx.Set<TargyTipus>();

        public TargyTipus GetOne(int id) => GetAll().SingleOrDefault(x => x.id == id);

        public void Update(TargyTipus entity)
        {
            var akt_tt = ctx.TargyTipus.First(x => x.id == entity.id);
            akt_tt.id = entity.id;
            akt_tt.nev = entity.nev;

            ctx.SaveChanges();
        }
    }
}
