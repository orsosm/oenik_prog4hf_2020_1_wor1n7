﻿// <copyright file="ExtraFoglalkozasokTipusRepository.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.RP
{
    using System;
    using System.Linq;
    using MyTanoda.DA;

    /// <summary>
    /// Public class for ExtraFoglalkozasokTipus repository.
    /// </summary>
    public class ExtraFoglalkozasokTipusRepository : IRepository<ExtraFoglalkozasokTipus>
    {
        private readonly MyTanodaDBEntities ctx = new MyTanodaDBEntities();

        public void Create(ExtraFoglalkozasokTipus entity)
        {
            ctx.ExtraFoglalkozasokTipus.Add(entity);
            ctx.SaveChanges();
        }

        public void Delete(int id)
        {
            ExtraFoglalkozasokTipus eft = ctx.ExtraFoglalkozasokTipus.Where(d => d.id == id).First();
            ctx.ExtraFoglalkozasokTipus.Remove(eft);

            try
            {
                ctx.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception("Adatbázis hiba");
            }
        }

        public IQueryable<ExtraFoglalkozasokTipus> GetAll() => ctx.Set<ExtraFoglalkozasokTipus>();

        public ExtraFoglalkozasokTipus GetOne(int id) => GetAll().SingleOrDefault(x => x.id == id);

        public void Update(ExtraFoglalkozasokTipus entity)
        {
            var akt_eft = ctx.ExtraFoglalkozasokTipus.First(x => x.id == entity.id);
            akt_eft.id = entity.id;
            akt_eft.nev = entity.nev;

            ctx.SaveChanges();
        }
    }
}
