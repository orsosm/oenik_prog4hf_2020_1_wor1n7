﻿// <copyright file="ExtraResztvevokRepository.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.RP
{
    using System;
    using System.Linq;
    using MyTanoda.DA;

    /// <summary>
    /// Public class for ExtraResztvevok repository.
    /// </summary>
    public class ExtraResztvevokRepository : IRepository<ExtraResztvevok>
    {
        private readonly MyTanodaDBEntities ctx = new MyTanodaDBEntities();

        public void Create(ExtraResztvevok entity)
        {
            ctx.ExtraResztvevok.Add(entity);
            ctx.SaveChanges();
        }

        public void Delete(int id)
        {
            ExtraResztvevok resztvevok = ctx.ExtraResztvevok.Where(d => d.id == id).First();
            ctx.ExtraResztvevok.Remove(resztvevok);

            try
            {
                ctx.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception("Adatbázis hiba");
            }
        }

        public IQueryable<ExtraResztvevok> GetAll() => ctx.Set<ExtraResztvevok>();

        public ExtraResztvevok GetOne(int id) => GetAll().SingleOrDefault(x => x.id == id);

        public void Update(ExtraResztvevok entity)
        {
            var akt_resztvevok = ctx.ExtraResztvevok.First(x => x.id == entity.id);
            akt_resztvevok.id = entity.id;
            akt_resztvevok.extra_id = entity.extra_id;
            akt_resztvevok.diak_id = entity.diak_id;
            akt_resztvevok.tanar_id = entity.tanar_id;

            ctx.SaveChanges();
        }
    }
}
