﻿// <copyright file="OrakRepository.cs" company="Orsós Miklós - WOR1N7">
// Copyright (c) Orsós Miklós - WOR1N7. All rights reserved.
// </copyright>

namespace MyTanoda.RP
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyTanoda.DA;

    /// <summary>
    /// Public class for Orak repository.
    /// </summary>
    public class OrakRepository : IRepository<Orak>
    {
        private readonly MyTanodaDBEntities ctx = new MyTanodaDBEntities();

        public void Create(Orak entity)
        {
            ctx.Orak.Add(entity);
            ctx.SaveChanges();
        }

        public void Delete(int id)
        {
            Orak ora = ctx.Orak.Where(d => d.id == id).First();
            ctx.Orak.Remove(ora);

            try
            {
                ctx.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception("Adatbázis hiba");
            }
        }

        public IQueryable<Orak> GetAll() => ctx.Set<Orak>();

        public Orak GetOne(int id) => GetAll().SingleOrDefault(x => x.id == id);

        public void Update(Orak entity)
        {
            var akt_ora = ctx.Orak.First(x => x.id == entity.id);
            akt_ora.ora = entity.ora;
            akt_ora.tanar = entity.tanar;
            akt_ora.diak = entity.diak;
            akt_ora.targy = entity.targy;
            akt_ora.tipus = entity.tipus;
            akt_ora.het_napja = entity.het_napja;

            ctx.SaveChanges();
        }
    }
}
